from os.path import join, splitext
import json

lyrics_input = "lyrics_input"
lyrics_output = "lyrics"

def get_artist(lyric):
	artist = ""
	for c in lyric:
		if c == "-":
			return artist[:-1]
		artist = artist + c

def get_title(lyric):
	title = ""
	title_bool = False
	for c in lyric:
		if c == "\n":
			return title[1:]
		if title_bool:
			title = title + c
		if c == "-":
			title_bool = True

def get_lyrics_index(lyrics_paths, running_id):
	lyrics_paths_dict = {"index" : []}
	identifier = 0
	for lyric_path in lyrics_paths:
		with open(join(lyrics_input, lyric_path),"r") as file:
			lyric = file.read()
		artist = get_artist(lyric)
		title = get_title(lyric)
		instance = { 
			"key" : running_id,
			"artist": artist,
			"title": title,
			"path": join(splitext(lyric_path)[0].replace(" ","")) + ".json"
		}
		running_id += 1
		lyrics_paths_dict["index"].append(instance)
	with open(join(lyrics_output,"lyrics_index.json"), "w") as write_file:
	    json.dump(lyrics_paths_dict, write_file)
	return lyrics_paths_dict, running_id

def get_starting_note(lyric):
	starting_note = ""
	starting_not_bool = False
	for c in lyric:
		if c == "}":
			return starting_note
		if starting_not_bool:
			starting_note = starting_note + c
		if c == "{":
			starting_not_bool = True

def get_structure(lyric):
	structure = []
	element = ""
	element_bool = False

	for c in lyric:
		if c == ">":
			element_bool= False
			element = element[0].upper() + element[1:]
			structure.append(element)
			element = ""
		if element_bool:
			element = element + c
		if c == "<":
			element_bool = True

	return structure
	
def get_chords(lyric):
	chords = []
	current_chord = ""
	chord_bool = False

	for c in lyric:
		if c == "]":
			chord_bool= False
			current_chord = current_chord[0].upper() + current_chord[1:]
			chords.append(current_chord)
			current_chord = ""
		if chord_bool:
			current_chord = current_chord + c
		if c == "[":
			chord_bool = True

	return list(dict.fromkeys(chords))