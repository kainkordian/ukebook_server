from os import listdir
from os.path import join, splitext
import json

from utils import *

lyrics_input = "lyrics_input"
lyrics_output = "lyrics"
running_id = 0
lyrics_paths = listdir(lyrics_input)

lyrics_index, running_id = get_lyrics_index(lyrics_paths, running_id)

def find_next_section(i):
	try :
		while lyric[i] != "<":
			i += 1
		return i
	except IndexError:
		return i

def find_end_of_row(i):
	try:
		while section_lyric[i] != "\n":
			i += 1
		return i
	except:
		return i

for lyric_path in lyrics_paths:
	with open(join(lyrics_input, lyric_path),"r") as file:
		lyric = file.read()
	lyric_dict = {}
	# lyric_dict["key"] = running_id
	# running_id += 1
	
	lyric_dict["header"] = {}
	lyric_dict["body"] = []

	lyric_dict["header"]["artist"] = get_artist(lyric)
	lyric_dict["header"]["title"] = get_title(lyric)
	lyric_dict["header"]["start"] = get_starting_note(lyric)
	lyric_dict["header"]["chords"] = get_chords(lyric)

	body = []
	i = 0
	section_buffer = ""

	while i < len(lyric):
		next_section_i = find_next_section(i)
		i = next_section_i

		section = {}
		section["key"] = running_id
		running_id += 1

		i += 1
		try:
			while lyric[i] != ">":
				section_buffer += lyric[i]
				i += 1
		except IndexError:
			pass
		i += 2 # skip ">" and "\n"

		section_name = section_buffer
		section_buffer = ""

		end_of_section = find_next_section(i)
		section_lyric = lyric[i:end_of_section]
		if len(section_lyric) == 0:
			continue
		if section_lyric[-2] == "\n":
			section_lyric = lyric[i:end_of_section-2]
		if section_lyric[-1] == "\n":
			section_lyric = lyric[i:end_of_section-1]

		section["section_name"] = section_name
		section_body = []
		
		# row routine
		j = 0 
		while j < len(section_lyric):
			end_of_row = find_end_of_row(j)
			row_lyric = section_lyric[j:end_of_row]
			j = end_of_row+1

			row = {}
			row["key"] = running_id
			running_id += 1
			row_body = []

			element_lyric = ""
			k = 0
			while k < len(row_lyric):

				try:
					while row_lyric[k] != "[":
						element_lyric += row_lyric[k]
						k += 1
				except IndexError:
					pass
				if element_lyric == "":
					pass
				else:
					chord = ""
					text = ""
					if element_lyric[0] == "[":
						l = 1
						while element_lyric[l] != "]":
							chord += element_lyric[l]
							l += 1
						l += 1
						chord = chord[0].upper() + chord[1:]
						text = element_lyric[l:]
					else:
						text = element_lyric
					element = {
						"key" : running_id,
						"chord" : chord,
						"text" : text,
					}
					running_id += 1
					row_body.append(element)
				element_lyric = "["
				k += 1
			i += 1
			row["body"] = row_body
			section_body.append(row)
		section["body"] = section_body
		body.append(section)
		lyric_dict["body"] = body
	with open(join(lyrics_output,splitext(lyric_path)[0]).replace(" ","") + ".json", "w") as write_file:
		json.dump(lyric_dict, write_file)